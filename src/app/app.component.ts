import { Component, OnInit } from '@angular/core';
import { IndexService } from '../service/service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [IndexService]
})
export class AppComponent implements OnInit {
  constructor(
    private IndexService: IndexService
  ) {}
  public ngOnInit() {
    this.IndexService.getLogin().subscribe(data => {
      console.log('res', data);
    });
  }
}
