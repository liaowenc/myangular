import {Injectable} from '@angular/core';
import { Todo } from '../app/todo';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Observable, of} from 'rxjs';


@Injectable({
  providedIn: 'root'
})

export class IndexService {
  constructor(private http: HttpClient) {}

  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };
  url = 'http://192.168.1.81/liding';

  // denglu
  // public anyList:any
  getLogin(data): Observable<Todo> {
    return this.http.post(this.url + '/admin/account/login.json', data);
  }
}
